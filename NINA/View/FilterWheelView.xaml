﻿<!--
    Copyright © 2016 - 2019 Stefan Berg <isbeorn86+NINA@googlemail.com>

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    N.I.N.A. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    N.I.N.A. is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with N.I.N.A..  If not, see http://www.gnu.org/licenses/.-->
<UserControl
    x:Class="NINA.View.FilterWheelView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
    xmlns:local="clr-namespace:NINA.View"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:ninactrl="clr-namespace:NINACustomControlLibrary;assembly=NINACustomControlLibrary"
    xmlns:ns="clr-namespace:NINA.Locale"
    xmlns:util="clr-namespace:NINA.Utility"
    d:DesignHeight="300"
    d:DesignWidth="300"
    mc:Ignorable="d">
    <GroupBox>
        <GroupBox.Header>
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="2*" />
                    <ColumnDefinition Width="3*" />
                </Grid.ColumnDefinitions>
                <TextBlock
                    VerticalAlignment="Center"
                    FontSize="20"
                    Text="{ns:Loc LblFilterWheel}" />
                <Grid Grid.Column="1" Margin="5">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="40" />
                        <ColumnDefinition />
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="Auto" />
                    </Grid.ColumnDefinitions>
                    <ninactrl:LoadingControl
                        Height="30"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        LoadingImageBrush="{StaticResource PrimaryBrush}"
                        Visibility="{Binding ChooseFWCommand.Execution.IsNotCompleted, Converter={StaticResource BooleanToVisibilityCollapsedConverter}, FallbackValue=Collapsed}" />
                    <ComboBox
                        Grid.Column="1"
                        MinHeight="40"
                        DisplayMemberPath="Name"
                        IsEnabled="{Binding FilterWheelInfo.Connected, Converter={StaticResource InverseBooleanConverter}}"
                        ItemsSource="{Binding FilterWheelChooserVM.Devices}"
                        SelectedItem="{Binding FilterWheelChooserVM.SelectedDevice}"
                        SelectedValuePath="Name" />
                    <Button
                        Grid.Column="2"
                        Width="40"
                        Height="40"
                        Margin="1,0,0,0"
                        Command="{Binding FilterWheelChooserVM.SetupDialogCommand}"
                        IsEnabled="{Binding FilterWheelChooserVM.SelectedDevice.HasSetupDialog}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="False">
                                <TextBlock Text="{ns:Loc LblSettings}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Grid>
                            <Path
                                Margin="5"
                                Data="{StaticResource SettingsSVG}"
                                Fill="{StaticResource ButtonForegroundBrush}"
                                Stretch="Uniform" />
                        </Grid>
                    </Button>
                    <Button
                        Grid.Column="3"
                        Width="40"
                        Height="40"
                        Margin="1,0,0,0"
                        Command="{Binding RefreshFWListCommand}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="False">
                                <TextBlock Text="{ns:Loc LblRescanDevices}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Grid>
                            <Path
                                Margin="5"
                                Data="{StaticResource LoopSVG}"
                                Fill="{StaticResource ButtonForegroundBrush}"
                                Stretch="Uniform" />
                        </Grid>
                    </Button>
                    <Grid
                        Grid.Column="4"
                        Width="40"
                        Height="40"
                        Margin="1,0,0,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center">
                        <ninactrl:CancellableButton
                            ButtonForegroundBrush="{StaticResource ButtonForegroundDisabledBrush}"
                            ButtonImage="{StaticResource PowerSVG}"
                            CancelButtonImage="{StaticResource CancelSVG}"
                            CancelCommand="{Binding CancelChooseFWCommand}"
                            Command="{Binding ChooseFWCommand}"
                            IsEnabled="{Binding FilterWheelInfo.Connected, Converter={StaticResource InverseBooleanConverter}}"
                            ToolTip="{ns:Loc LblConnect}"
                            Visibility="{Binding FilterWheelInfo.Connected, Converter={StaticResource InverseBoolToVisibilityConverter}}" />
                        <Grid Visibility="{Binding FW, Converter={StaticResource NullToVisibilityConverter}}">
                            <Button
                                Command="{Binding DisconnectCommand}"
                                IsEnabled="{Binding FilterWheelInfo.Connected}"
                                Visibility="{Binding FilterWheelInfo.Connected, Converter={StaticResource VisibilityConverter}}">
                                <Button.ToolTip>
                                    <ToolTip ToolTipService.ShowOnDisabled="False">
                                        <TextBlock Text="{ns:Loc LblDisconnect}" />
                                    </ToolTip>
                                </Button.ToolTip>
                                <Grid>
                                    <Path
                                        Margin="5"
                                        Data="{StaticResource PowerSVG}"
                                        Fill="{StaticResource ButtonForegroundBrush}"
                                        Stretch="Uniform" />
                                </Grid>
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </GroupBox.Header>
        <Grid Background="{StaticResource BackgroundBrush}">
            <StackPanel>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblName}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding FW.Name}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblDescription}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding FW.Description}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblDriverInfo}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding FW.DriverInfo}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblDriverVersion}" />
                            <TextBlock Margin="5,0,0,0" Text="{Binding FW.DriverVersion}" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <UniformGrid Margin="0,5,0,0" Columns="2">
                    <ComboBox
                        Name="PART_FilterComboBox"
                        IsEnabled="{Binding FilterWheelInfo.IsMoving, Converter={StaticResource InverseBooleanConverter}}"
                        ItemsSource="{Binding FW.Filters}"
                        SelectedValue="{Binding TargetFilter}">
                        <ComboBox.ItemTemplate>
                            <DataTemplate>
                                <TextBlock Text="{Binding Name}" />
                            </DataTemplate>
                        </ComboBox.ItemTemplate>
                    </ComboBox>
                    <Button Command="{Binding ChangeFilterCommand}">
                        <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblChange}" />
                    </Button>
                </UniformGrid>
                <ListView
                    Name="PART_FilterListView"
                    MinHeight="200"
                    Margin="0,10,0,0"
                    IsEnabled="{Binding FilterWheelInfo.IsMoving, Converter={StaticResource InverseBooleanConverter}}"
                    IsHitTestVisible="False"
                    ItemsSource="{Binding FW.Filters}"
                    SelectedItem="{Binding FilterWheelInfo.SelectedFilter}">
                    <ListView.View>
                        <GridView>
                            <GridViewColumn
                                Width="Auto"
                                DisplayMemberBinding="{Binding Name}"
                                Header="{ns:Loc LblFilterName}" />
                        </GridView>
                    </ListView.View>
                </ListView>
            </StackPanel>
        </Grid>
    </GroupBox>
</UserControl>